﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace idleMines
{
    public class MineSystem : MonoBehaviour
    {
        [SerializeField] GameObject mineLevelPrefab;
        [SerializeField] RectTransform workerContainer;
        [SerializeField] DropOffSpot liftDropoffSpot;
        [SerializeField] PickUpSpot lowestPickUpSpot;
        [SerializeField] float mineLevelHeight;
        [SerializeField] CurrencyAmount mineCost;
        [SerializeField] TextMeshProUGUI costText;
        [SerializeField] List<FacilityStats> mineLevelStats;

        protected AccountBalance account;
        Button button;
        int currentMineIndex = 0;

        private void Start()
        {
            account = FindObjectOfType<AccountBalance>();
            account.OnBalanceChanged += CheckBuyable;
            button = GetComponent<Button>();
            button.onClick.AddListener(BuyNewMineLevel);

            UpdateCostText();
            CheckBuyable();
        }

        private void UpdateCostText()
        {
            costText.text = mineCost.ToString();
        }

        private void CheckBuyable()
        {
            button.interactable = account.CanCover(mineCost);

            gameObject.SetActive(currentMineIndex < mineLevelStats.Count - 1);
        }

        private void BuyNewMineLevel()
        {
            if (!account.CanCover(mineCost)) return;

            currentMineIndex++;
            account.DeductBalance(mineCost);
            CurrencyTransferFacility newMine = Instantiate(mineLevelPrefab, transform.position, Quaternion.identity, transform.parent).GetComponent<CurrencyTransferFacility>();
            newMine.Setup(mineLevelStats[currentMineIndex], liftDropoffSpot, workerContainer);

            lowestPickUpSpot.NextTarget = newMine.PickUpSpot;
            lowestPickUpSpot = newMine.PickUpSpot;

            MoveButtonUpdateCost();
        }

        private void MoveButtonUpdateCost()
        {
            transform.Translate(new Vector3(0, -mineLevelHeight));
            mineCost.AddBalance(mineCost);
            UpdateCostText();

            CheckBuyable();
        }
    }
}

