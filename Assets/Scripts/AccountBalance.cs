﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace idleMines
{

    public class AccountBalance : MonoBehaviour
    {
        public delegate void BalanceEvent();
        public event BalanceEvent OnBalanceChanged;

        CurrencyAmount balance;
        TextMeshProUGUI cashBalanceText;

        private void Awake()
        {
            balance = new CurrencyAmount();
        }

        private void Start()
        {
            cashBalanceText = GetComponent<TextMeshProUGUI>();
            BalanceUpdated();
        }

        public void AddBalance(CurrencyAmount currency)
        {
            balance.AddBalance(currency);
            BalanceUpdated();
        }

        public CurrencyAmount DeductBalance(CurrencyAmount currency)
        {
            CurrencyAmount deducted = balance.DeductBalance(currency);
            BalanceUpdated();
            return deducted;
        }   

        public bool CanCover(CurrencyAmount currencyAmount)
        {
            return balance.BiggerThan(currencyAmount) || balance.Equals(currencyAmount);
        }

        private void BalanceUpdated()
        {
            OnBalanceChanged?.Invoke();
            cashBalanceText.text = balance.ToString();
        }

        public void ResetAll()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void Add100K()
        {
            balance.AddBalance(new CurrencyAmount(100, 1));
            BalanceUpdated();
        }
    }
}