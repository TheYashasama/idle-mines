﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace idleMines
{

    public class CurrencySource : MonoBehaviour, IPickUp
    {

        [SerializeField] PickUpSpot pickUpSpot;

        private void Start()
        {
            pickUpSpot.SetPickUp(this);
        }

        public CurrencyAmount PickUp(CurrencyAmount currencyAmount)
        {
            return currencyAmount;
        }
    }
}
