﻿
namespace idleMines
{

    [System.Serializable]
    public class CurrencyAmount
    {
        private static string[] unitStrings = new string[8] { "", "K", "M", "B", "T", "aa", "ab", "ac" };

        public float Amount;
        public int Unit;


        public CurrencyAmount()
        {
            Amount = 0;
            Unit = 0;
        }

        public CurrencyAmount(float amount, int unit)
        {
            Amount = amount;
            Unit = unit;
        }

        public void Clear()
        {
            Amount = 0;
            Unit = 0;
        }

        public CurrencyAmount AddBalance(CurrencyAmount currency)
        {
            float add = ConvertCurrencyAmountToCurrentUnit(currency);
            Amount = VerifyBalance(Amount, add);
            return this;
        }

        public CurrencyAmount DeductBalance(CurrencyAmount currency)
        {
            float deduct = ConvertCurrencyAmountToCurrentUnit(currency);
            int deductUnit = Unit;
            float previousAmount = Amount; 
            Amount = VerifyBalance(Amount, -deduct);  

            if (deduct > previousAmount) deduct = previousAmount;
            return new CurrencyAmount(deduct, deductUnit); 
        }


        // A: 500, U: 0 / CA: 1 / CU: 1
        float ConvertCurrencyAmountToCurrentUnit(CurrencyAmount currency)
        {
            float value = currency.Amount;
            int otherUnit = currency.Unit;
            while (otherUnit != Unit)
            {
                value *= otherUnit < Unit ? 0.001f : 1000;
                otherUnit += otherUnit < Unit ? 1 : -1;
            }
            return value;
        }

        public float VerifyBalance(float previousBalance, float difference)
        {
            float newBalance = previousBalance + difference;
            if (newBalance < 0) return 0;
            newBalance = UpdateUnit(newBalance);

            return newBalance;
        }

        private float UpdateUnit(float balance)
        {
            while (balance >= 1000)
            {
                Unit++;
                balance /= 1000;
            }
            if (balance < 1 && Unit > 0)
            {
                Unit--;
                balance *= 1000;
            }

            if (balance <= 0) Unit = 0;

            return balance;
        }

        public bool Equals(CurrencyAmount currencyAmount)
        {
            return 0 == Compare(currencyAmount);
        }

        public bool SmallerThan(CurrencyAmount currencyAmount)
        {
            return -1 == Compare(currencyAmount);
        }

        public bool BiggerThan(CurrencyAmount currencyAmount)
        {
            return 1 == Compare(currencyAmount);
        }

        int Compare(CurrencyAmount other)
        {
            if (other.Unit == Unit)
            {
                if (Amount == other.Amount) return 0;
                else if (Amount < other.Amount) return -1;
                else if (Amount > other.Amount) return 1;
            }
            else if (Unit < other.Unit)
            {
                return -1;
            }
            else if (Unit > other.Unit)
            {
                return 1;
            }
            return 0;
        }

        public CurrencyAmount Clone()
        {
            return new CurrencyAmount(Amount, Unit);
        }

        public override string ToString()
        {
            string unitString = Unit < unitStrings.Length ? unitStrings[Unit] : "§";
            string cashString = Amount.ToString("F2");
            return $"$ {cashString} {unitString}";
        }
    }
}
