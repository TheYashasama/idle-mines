﻿using UnityEngine;
using UnityEditor;
namespace idleMines
{
    // using the example from https://docs.unity3d.com/ScriptReference/PropertyDrawer.html
    [CustomPropertyDrawer(typeof(CurrencyAmount))]
    public class CurrencyAmountEditor : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var amountLabelRect =  new Rect(position.x, position.y, 50, position.height);
            var amountRect = new Rect(position.x + 55, position.y, 50, position.height);
            var unitLabelRect = new Rect(position.x + 110, position.y, 50, position.height);
            var unitRect = new Rect(position.x + 165, position.y, 50, position.height);

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            EditorGUI.LabelField(amountLabelRect, "Amount: ");
            EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("Amount"), GUIContent.none);
            EditorGUI.LabelField(unitLabelRect, "Unit: ");
            EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("Unit"), GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}