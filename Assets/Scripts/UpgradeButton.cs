﻿using UnityEngine;
using UnityEngine.UI;
namespace idleMines
{

    public class UpgradeButton : MonoBehaviour
    {
        WorkerController upgradeTarget;
        Button button;

        private void OnEnable()
        {
            FindObjectOfType<AccountBalance>().OnBalanceChanged += CheckUpgradeable;
            button = GetComponent<Button>();
            button.onClick.AddListener(Upgrade);
        }

        private void OnDisable()
        {
            if (button) button.onClick.RemoveAllListeners();
        }

        public void SetTarget(WorkerController upgradeable)
        {
            upgradeTarget = upgradeable;
            CheckUpgradeable();
        }

        private void CheckUpgradeable()
        {
            if (upgradeTarget != null) button.interactable = upgradeTarget.CanBeUpgraded();
        }

        void Upgrade()
        {
            if (upgradeTarget != null) upgradeTarget.Upgrade();
            CheckUpgradeable();
        }
    }
}
