﻿using System.Collections.Generic;
using UnityEngine;

namespace idleMines
{

    public class Silo : WorkerController, IDropOff
    {
        new protected void Start()
        {
            base.Start();
            dropOffSpot.SetDropoff(this);
        }

        public void DropOff(CurrencyAmount currencyAmount)
        {
            account.AddBalance(currencyAmount);
        }

    }
}
