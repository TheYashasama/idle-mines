﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace idleMines
{
    public class WorkerTarget : MonoBehaviour
    {

        [SerializeField] WorkerTarget nextTarget;

        public WorkerTarget NextTarget
        {
            get { return nextTarget; }
            set
            {
                if (value == nextTarget) return;
                nextTarget = value;
            }
        }
    }
}
