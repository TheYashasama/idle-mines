﻿namespace idleMines
{
    public class DropOffSpot : WorkerTarget
    {
        private IDropOff dropOff;

        public void SetDropoff(IDropOff dropOff)
        {
            this.dropOff = dropOff;
        }

        public void DropOff(CurrencyAmount currencyAmount)
        {
            dropOff.DropOff(currencyAmount);
        }
    }
}
