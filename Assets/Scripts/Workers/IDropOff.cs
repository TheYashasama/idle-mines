﻿namespace idleMines
{
    public interface IDropOff
    {
        void DropOff(CurrencyAmount currencyAmount);
    }
}