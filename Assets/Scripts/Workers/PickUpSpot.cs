﻿namespace idleMines
{
    public class PickUpSpot : WorkerTarget
    {
        private IPickUp pickUp;

        public void SetPickUp(IPickUp dropOff)
        {
            this.pickUp = dropOff;
        }

        public CurrencyAmount PickUp(CurrencyAmount currencyAmount)
        {
            return pickUp.PickUp(currencyAmount);
        }
    }
}