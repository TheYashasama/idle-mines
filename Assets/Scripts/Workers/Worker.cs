﻿using System.Collections;
using TMPro;
using UnityEngine;
namespace idleMines
{

    public class Worker : MonoBehaviour
    {

        [System.Serializable]
        public struct WorkerStats
        {
            public float Speed;
            public CurrencyAmount Capacity;
        }

        TextMeshProUGUI loadText;
        CurrencyAmount load;

        WorkerStats stats;
        DropOffSpot finalDropOff;

        void Start()
        {
            load = new CurrencyAmount();
            loadText = GetComponentInChildren<TextMeshProUGUI>();
            UpdateText();
        }

        public void Initialize(WorkerStats stats, WorkerTarget firstTarget, DropOffSpot finalDropOff)
        {
            this.stats = stats;
            this.finalDropOff = finalDropOff;
            StartCoroutine(Move(firstTarget));
        }

        public void Upgrade(WorkerStats stats)
        {
            this.stats = stats;
        }

        void UpdateText()
        {
            loadText.text = load.ToString();
        }

        bool AtFullCapacity()
        {
            return load.BiggerThan(stats.Capacity) || load.Equals(stats.Capacity);
        }

        // -------------------------- State Mangement -------------------------- 

        IEnumerator Move(WorkerTarget targetSpot)
        {
            Transform targetTransform = targetSpot.transform;

            Vector3 direction = targetTransform.position - transform.position;
            Vector3 heading = direction.normalized;
            while (Vector3.Angle(direction, heading) < 90)
            {
                transform.Translate(heading * Time.deltaTime * stats.Speed);
                yield return new WaitForEndOfFrame();

                direction = targetTransform.position - transform.position;
            }

            if (targetSpot as DropOffSpot)
            {
                StartCoroutine(DropOffCurrency(targetSpot as DropOffSpot));
            }
            else if (targetSpot as PickUpSpot)
            {
                StartCoroutine(PickupCurrency(targetSpot as PickUpSpot));
            }
        }

        IEnumerator PickupCurrency(PickUpSpot pickUpSpot)
        {
            CurrencyAmount freeCapacity = stats.Capacity.Clone();
            freeCapacity.DeductBalance(load);
            CurrencyAmount pickedUp = pickUpSpot.PickUp(freeCapacity);
            load.AddBalance(pickedUp);

            UpdateText();

            yield return new WaitForEndOfFrame();

            if (AtFullCapacity())
            {
                StartCoroutine(Move(finalDropOff));
            }
            else
            {
                StartCoroutine(Move(pickUpSpot.NextTarget));
            }
        }

        IEnumerator DropOffCurrency(DropOffSpot dropOffSpot)
        {
            dropOffSpot.DropOff(load);
            load.Clear();
            UpdateText();
            yield return new WaitForEndOfFrame();
            StartCoroutine(Move(dropOffSpot.NextTarget));
        }
    }
}
