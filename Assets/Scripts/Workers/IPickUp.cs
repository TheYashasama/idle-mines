﻿namespace idleMines
{
    public interface IPickUp
    {
        CurrencyAmount PickUp(CurrencyAmount currencyAmount);
    }
}