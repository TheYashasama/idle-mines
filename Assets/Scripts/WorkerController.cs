﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace idleMines
{
    public class WorkerController : MonoBehaviour
    {
        [Header("WorkerController Configuration")]
        [SerializeField] GameObject workerPrefab;
        [SerializeField] protected DropOffSpot dropOffSpot;
        [SerializeField] WorkerTarget workerTarget;
        [SerializeField] protected RectTransform workerContainer;


        [Header("Upgradeable Configuration")]
        [SerializeField] UpgradeButton upgradeButton;
        [SerializeField] TextMeshProUGUI costText;
        [SerializeField] protected FacilityStats stats;
        [SerializeField] int maxUpgradeLevel = 10;

        Worker.WorkerStats workerStats;
        int workerNumber;
        CurrencyAmount upgradeCost;
        int nextWorkerAtLevel;

        int upgradeLevel = 1;
        protected AccountBalance account;
        List<Worker> workers;

        private void Awake()
        {
            workers = new List<Worker>();
        }

        protected void Start()
        {
            account = FindObjectOfType<AccountBalance>();

            InitializeStats();
            upgradeButton.SetTarget(this);

            for (int i = 0; i < workerNumber; i++)
            {
                AddWorker();
            }
            UpdateCostText();
        }

        protected void InitializeStats()
        {
            workerStats = stats.WorkerStats;
            workerNumber = stats.WorkerNumber;
            upgradeCost = stats.UpgradeCost.Clone();
            nextWorkerAtLevel = stats.NextWorkerAtLevel;
        }

        void AddWorker()
        {
            Worker worker = Instantiate(workerPrefab, dropOffSpot.transform.position, Quaternion.identity, workerContainer).GetComponent<Worker>();
            workers.Add(worker);
            worker.Initialize(workerStats, workerTarget, dropOffSpot);
        }

        public void Upgrade()
        {
            if (!CanBeUpgraded()) return;
            account.DeductBalance(upgradeCost);
            UpgradeAllStats();
        }

        private void UpgradeAllStats()
        {
            upgradeLevel++;
            workerStats.Capacity.AddBalance(workerStats.Capacity);
            workerStats.Speed *= 1.2f;
            upgradeCost.AddBalance(upgradeCost);
            UpdateCostText();

            ApplyStatsToWorkers();

            if (upgradeLevel == nextWorkerAtLevel)
            {
                AddWorker();
                nextWorkerAtLevel *= 2;
            }
        }

        protected void ApplyStatsToWorkers()
        {
            foreach (var worker in workers)
            {
                worker.Upgrade(workerStats);
            }
        }

        private void UpdateCostText()
        {
            costText.text = upgradeCost.ToString();
        }

        public bool CanBeUpgraded()
        {
            return upgradeLevel < maxUpgradeLevel && account.CanCover(upgradeCost);
        }
    }
}
