﻿using TMPro;
using UnityEngine;

namespace idleMines
{
    public class CurrencyTransferFacility : WorkerController, IDropOff, IPickUp
    {
        [Header("CurrencyTransferFacility Configuration")]
        [SerializeField] PickUpSpot pickUpSpot;
        public PickUpSpot PickUpSpot {  get { return pickUpSpot; } }

        [SerializeField] TextMeshProUGUI currencyText;
        CurrencyAmount balance;

        new protected void Start()
        {
            base.Start();

            balance = new CurrencyAmount();
            dropOffSpot.SetDropoff(this);
            pickUpSpot.SetPickUp(this);
            UpdateText();
        }

        public void Setup(FacilityStats stats, WorkerTarget nextTarget, RectTransform workerContainer)
        {
            this.stats = stats;
            InitializeStats();
            this.workerContainer = workerContainer;
            pickUpSpot.NextTarget = nextTarget;
            ApplyStatsToWorkers();
        }

        public void DropOff(CurrencyAmount currencyAmount)
        {
            balance.AddBalance(currencyAmount);
            UpdateText();
        }

        private void UpdateText()
        {
            currencyText.text = balance.ToString();
        }

        public CurrencyAmount PickUp(CurrencyAmount currencyAmount)
        {
            CurrencyAmount deducted = balance.DeductBalance(currencyAmount);
            UpdateText();
            return deducted;
        }
    }
}
