﻿using UnityEngine;

namespace idleMines
{
    [CreateAssetMenu(menuName = "idleMines/FacilityStats")]
    public class FacilityStats : ScriptableObject
    {

        [SerializeField] Worker.WorkerStats workerStats;
        public Worker.WorkerStats WorkerStats
        {
            get
            {
                Worker.WorkerStats stats = new Worker.WorkerStats();
                stats.Capacity = workerStats.Capacity.Clone();
                stats.Speed = workerStats.Speed;
                return stats; 
            }
        }
        public int WorkerNumber;
        public CurrencyAmount UpgradeCost;
        public int NextWorkerAtLevel;
    }
}
